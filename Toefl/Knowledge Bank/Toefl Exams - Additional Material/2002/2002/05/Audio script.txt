
M: I think I'll run down to the bookstore and get a few things 
W: But aren't we going to meet Shirley at the student center? She is 
expecting us at 4. And it's almost that now.  
W: Congratulations! I heard about your new job.  
M: Thanks. Nearly perfect for me. It's really interesting. The hours are  
ideal and it's an easy walk from home. If only the pay were half as good  
as everything else.  
 W: Do you know anyone who is driving to the conference in Boston  
next weekend?  
M: Pete is. I think he has room for another person.  
M: Did I hear you say you are using the newspaper in your political  
science class? I was wondering if when you finish it, you can pass it onto  
me?  
W: Well, we do use it in class. But I always read the copy in the library.  
5��W: I noticed you haven't been getting along well with your roommate  
lately.  
M: You got that right. And it's going to be a long time before I feel  
comfortable with him again.  
6��W: How did you ever manage to get through all 1000 pages of that new  
spy thriller?  
M: It took a while. But once I had started it, I couldn't put it down.  
7��M: Hi, XXX, how are you doing? I heard you had an operation over the  
spring break.  
W: Yes, XXX, thanks for asking. I was pretty much out of condition for a  
few weeks. Bit finally I'm back in my feet again.  
8��W: How about a little tennis? Say Saturday or Sunday?  
M: It sounds great. I could use a good workout. It seems like all I have  
been doing lately is sitting in front of this computer.  
9��W: Did you read today's newspaper? I heard there is something about  
a new wonder drug.  
M: I did read an article about medical researchers being on the verge of a  
major breakthrough.  
10.M: Where have you been? We were supposed to meet at the library half  
an hour ago.  
W: Yeah, I'm really sorry. I guess I just lost track of the time.  
11.M: I'm sorry. But you can't take your camera inside. You'll have  
to leave it here and pick it up after the concert.  
W: Well, I guess it's too late to take it back to the car. Please be  
careful with it. It belongs to my roommate. She'll never forgive me if  
anything happens to it.  
12.M: The application instructions say to enclose a check or money  
order for twenty dollars. But I don't have a checking account.  
W: You can just pick up a money order at the post office.  
13.M: You know, I really think you should run for class president.  
Everybody knows you and likes you. And you got some great ideas.  
W: Thanks. I have thought about it. I'm taking six classes and working a  
part-time job. That's about all I can handle right now.  
14.M: I'm having a few friends over for a lunch tomorrow. It'll be  
great if you can join us.  
W: I doubt I'll be able to make it. My brother is leaving for Chicago  
tomorrow afternoon. And I promised to give him a ride to the airport.  
15.W: My cousin Lisa said she mailed me some books. But they never  
came.  
M: Well, you just moved into a new dormitory. She probably sent them out  
before she had your new address.  
16.M: Excuse me. Do you have the time?  
W: Actually I'm not positive. But I'd say it's right around noon.  
17.    W: My dinner tastes kind of funny.  
M: Then why don't you have the waiter bring you something else.  
18.    W: Did Mary meet you at the airport yesterday?  
M: Yes. But she sure got tired waiting for my flight to get in. We circled  
the airport for three hours.  
19.    W: About this survey on the quality of life in the dorm. I feel sort  
of awkward because, well, I'm not rally comfortable here. Are you sure you  
want me to fill out this survey form?  
M: It's people like you who can help us target areas for improvement.  
20.    M: I'm the only one in class who didn't sign up for the biology  
trip. Slogging through a swamp in the rain can't gun fun.  
W: Nope. But I've got the feeling your classmates will come back knowing  
some things you won't know.  
21.    W: What are you watching?  
M: Some boring comedy show. But the Channel 6 news is on in a couple of  
minutes.  
22.    M: Excuse me, Professor Jones. I was absent from the first class and  
I heard that's when you handed out the course outline. Would you have an  
extra copy?  
W: I don't have anyone with me. There are a few left in my office. Why  
don't you stop by after class.  
23.    W: So, how did Jason's presentation go?  
M: It wasn't bad. But the topic he chose last time was more interesting.  
24.    M: Would you happen to know somebody who'd like to buy my car?  
W: Well, I don't know of anyone off hand. But I'll check with some of my  
friends.  
25.    W: Do you know where the nearest bus stop is?  
M: Actually I'm pretty new to the area.  
26.    M: Did you hear the weather report says we are going to get at least  
a foot of snow tomorrow?  
W: That much! That's incredible. I can't wait to get outside and play in  
it.  
27.    W: Growing up we never had a TV. Even now I'm not used to watching  
it much.  
M: Well, it's kind of like reading. Some things you find are great, But a  
lit are real waste of time. You have to pick and choose.  
28.    W: I live this hat. And look, it's on sale.  
M: Yes, but it doesn't do much for you. What about the green one? It's a  
little expensive. But it really looks great on you.  
29.    M: That's a really interesting shirt. Must be from your vacation.  
W: No. But you are close. My sister brought it back from Hawaii for me.  
30.    W: Your company isn't moving to the west coast after all.  
M: Well, not for the time being. But I've been looking into other  
employment opportunities here anyway. Just in case.  


31-34  
W: ok, last night you were supposed to read an article about human bones.  
Are there any comments about it?  
M: well, to begin with, I was surprised to find out there were so much  
going on in bones. I always assumed they were pretty lifeless.  
W: Well, that's an assumption many people make. But the fact is bones are  
made of dynamic living tissue that requires continuous maintenance and  
repair.  
M: Right. That's one of the things I found so fascinating about the  
article the way the bones repair themselves.  
W: Ok. So can you tell us how the bones repair themselves.  
M: Sure. See, there are two groups of different types of specialized cells  
in the bone that work together to do it. The first group goes to an area of  
the bone that needs repair. This group of cells produce the chemical that  
actually breaks down the bone tissue, and leaves a hole in it. After that  
the second group of specialized cells comes and produce the new tissue that  
fills in the hole that was made by the first group.  
W: Very good. This is a very complex process. In fact, the scientists who  
study human bones don't completely understand it yet. They are still  
trying to find out how it all actually works. Specifically, because  
sometimes after the first group of cells leaves a hole in the bone tissue,  
for some reason, the second group doesn't completely fill in the hole. And  
this can cause real problems. It can actually lead to a disease in which  
the bone becomes weak and is easily broken.  
M: ok, I get it. So if the scientists can figure out what makes the  
specialized cells work, maybe they can find a way to make sure the second  
group of cells completely fills the hole in the bone tissue every time.  
That'll prevent the disease from every occurring.  

35-38  
M: Hi Diana, mind if I sit down?  
W: Not at all, Jerry. How have you been?  
M: Good. But I'm surprised to see you on the city bus. Your car in the  
shop?  
W: No. I've just been thinking a lot about the environment lately. So I  
decided the air will be a lot cleaner if we call use public transportation  
when we could.  
M: I'm sure you are right. The diesel bus isn't exactly pollution free.  
W: True. They'll be running a lot cleaner soon. We were just talking about  
that in my environmental engineering class.  
M: What could the city do? Install pollution filters in all their buses?  
W: They could, but those filters make the engines work harder and really  
cut down on the fuel efficiency. Instead they found a way to make their  
engines more efficient.  
M: How?  
W: Well, there is a material called XXX. It's a really good insulator. And  
a think coat of it get sprayed on the certain part of the engine.  
M: An insulator?  
W: Yeah. What it does is reflect back the heat of burning fuel. So the fuel  
will burn much hotter and burn up more completely.  
M: So a lot less unburning fuel comes out to pollute the air.  
W: And the bus will need less fuel. So with the saving on fuel cost, they  
say this will all pay for itself in just six months.  
M: Sounds like people should all go out and get some this stuff to spray  
their car engines.  
W: Well, it's not really that easy. You see, normally, the materials are  
fine powder. To melt it so you can spray a coat of it on the engine parts,  
you first have to heat it over 10,000 degrees and then, well, you get the  
idea. It's not something you or I be able to do ourselves.  

31-34  
W: ok, last night you were supposed to read an article about human bones.  Are there any comments about it?  
M: well, to begin with, I was surprised to find out there were so much going on in bones. I always assumed they were pretty lifeless.  
W: Well, that's an assumption many people make. But the fact is bones are made of dynamic living tissue that requires continuous maintenance and repair.  
M: Right. That's one of the things I found so fascinating about the article the way the bones repair themselves.  
W: Ok. So can you tell us how the bones repair themselves.  
M: Sure. See, there are two groups of different types of specialized cells in the bone that work together to do it. The first group goes to an area of the bone that needs repair. This group of cells produce the chemical that actually breaks down the bone tissue, and leaves a hole in it. After that the second group of specialized cells comes and produce the new tissue that fills in the hole that was made by the first group.  
W: Very good. This is a very complex process. In fact, the scientists who study human bones don't completely understand it yet. They are still trying to find out how it all actually works. Specifically, because sometimes after the first group of cells leaves a hole in the bone tissue, for some reason, the second group doesn't completely fill in the hole. And this can cause real problems. It can actually lead to a disease in which the bone becomes weak and is easily broken.  
M: ok, I get it. So if the scientists can figure out what makes the specialized cells work, maybe they can find a way to make sure the second group of cells completely fills the hole in the bone tissue every time. That'll prevent the disease from every occurring.  

31. What is the discussion mainly about?

32. What is the function of the first group of specialized cells discussed in the talk?

33. What does the professor say about scientists who study the specialized cells in human bones?

34. According to the student, what is one important purpose of studying specialized cells in human bones?


35-38  
M: Hi Diana, mind if I sit down?  
W: Not at all, Jerry. How have you been?  
M: Good. But I'm surprised to see you on the city bus. Your car in the shop?  
W: No. I've just been thinking a lot about the environment lately. So I decided the air will be a lot cleaner if we all use public transportation when we could.  
M: I'm sure you are right. The diesel bus isn't exactly pollution free.  
W: True. But they'll be running a lot cleaner soon. We were just talking about that in my environmental engineering class.  
M: What's the city gonna do? Install pollution filters of some sort on their buses?  
W: They could, but those filters make the engines work harder and really cut down on fuel efficiency. Instead they found a way to make their engines more efficient.  
M: How?  
W: Well, there is a material called the coniine oxide. It's a really good insulator. And a thick coat of it get sprayed on the certain part of the engine.  
M: An insulator?  
W: Well, yeah. Actually, what it does is reflect back the heat of burning fuel. So the fuel will burn much hotter and burn up more completely.  
M: So a lot less unburned fuel comes out to pollute the air, right?
W: Yeah, and the bus will need less fuel. So with the savings on fuel cost, they say this will all pay for itself in just six months.  
M: Sounds like people should all go out and get some this stuff to spray their car engines.  
W: Well, it's not really that easy. You see, normally, the materials are fine powder. To melt it so you can spray a coat of it on the engine parts, you first have to heat it over 10,000 degrees and then, well, you get the idea. It's not something you or I be able to do ourselves. 
 
35. What is the conversation mainly about?

36. Why did the woman decide to ride the city bus?

37. What is the coniine oxide?

38. According to the woman, what may limit the use of the coniine oxide in cars?


39-42
Good evening, ladies and gentlemen. My name is Alice Brown. As you know, we hold a series of events during the school year on various culture topics. I am happy there's such a large crowd of both students and professors, that's it, the second of our time, our city art presentation this year. I see that almost every seat is taken. Tonight, we are lucky to have our guest, the man of considerable fame in the world of music. He began to play the piano at age of 5, by the time he was 10, he was already composing in playing his own pieces. He's a graduate of the famous Juliet School in New York City. Our guest has spent at least 45 years of his very successful career touring the world playing in concert. We are fortunate that he's consented to come share some of his experiences with us. He has had many adventures along the way, lost instruments, miss connections, no hotel room, locked concert halls, and so on. He's played for all of the well-known conductors, not only in North America but all over the world. The title of his talk is the country tour 40 years on 4 continents. Please join me in welcoming Mr. Daniel Robinson, one of the foremost pianists of our day.

39. What is the main purpose of Dean Brown's remarks?

40. Why does Dean Brown feel pleased?

41. When did the pianist begin to play his own composition?

42. What will the pianist mainly do?


43-45
      Let's turn our focus now to advertising. We all know what an advertisement is, it's essentially a message that announces something for sale. Now, there is an important precondition that must exist before you have advertising, and that's a large supply of consumer goods, that is, things to sell. You see, in the place with a demand for a product is greater than the supply, there's no need to advertise. Now, the early form of advertising going back many hundreds of years with a simple sign there were shop doors that told you whether the shop was a bakery, a butcher shop or what have you, then was the advent of the printing crest. Advertising increased substantially as for products like coffee, tea, and chocolate appeared in newspapers and another periodicals as well as on the side of the buildings. In the American colonies, advertising in communication's media like newspapers and pamphlets became a major factor in marketing goods and services. By modern standards, these early advertisements were quite small and subdue, not the splash sheet whole page spreads of today, still some of them appeared on the front page of newspapers, probably because the news often consist of less and fresh reports from distant Europe, for the ads were current or local. Advertising really came into its own and became a central part of doing business, during the industry revolution, suddenly there was a much greater supply of things to sell. And as we said earlier, that is the driving force behind advertising. People's attention had been drawn to the new product. Let's take a look at some of the advertisements from that time.

43. What is the main topic of the talk?

44. What does the speaker say is the important precondition for advertising?

45. According to the speaker, what was the first advertisement?


46-50
   We know then that in the US, it's the job of Congress to review propose new laws, which we call bills, and perhaps to modify these bills and then wrote on them. But even if the bill passed in Congress, it still doesn't become a law until the president had a chance to review it too. And if it's not to the president's liking, the bill can be vetoed or killed in either of two ways. One is by a veto message. The president has ten days to veto the bill by returning it to Congress, along with the message explaining why it's being rejected. This keeps the bill from becoming a law unless overwhelming majorities of both houses of Congress vote to over-right the president's veto. Something they really do. Often, lawmakers simply revised the vetoed bill and passed it again. This time, in the form the president less likely to object to, and us less likely to want to veto. The other way the president can kill a bill is by pocket veto. Here's what happen. If the president doesn't sign the bill within ten days, and Congress are jurors during that time, then the bill will not become law. Notice that is only the end of entire session of Congress that the pocket veto can be used, not just whenever Congress take the shorter break, say, for a summer vacation, after a pocket veto, that particular bill is dead. If a lawmaker in Congress want to push the matter in their next session, they'll have to start all over with a brand new version of the bill.

46. What is the main topic of the talk?

47. According to the speaker, what does the veto message explain?

48. According to the speaker, what do lawmakers often do after a veto message is issued?

49. What happens to a bill as a result of a pocket veto?

50.

ԭ����̫ɵ ����ѩ�� 

